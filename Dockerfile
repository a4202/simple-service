FROM theyoctojester/justnode:justnode-noicu-licenses

COPY dist/index.js /home/node/index.js

USER node

EXPOSE 3000

ENTRYPOINT [ "node", "/home/node/index.js" ]